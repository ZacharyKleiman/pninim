# Export Information

Tools are actively under development to export from Sefaria. Due to some issues I've been having getting things like Headings to convert Sefaria will be modified to automatically execute a parsing script which is currently under development which will do all the parsing I had trouble modifying Sefaria to do. The script will have various parsing modes depending on the exact text to convert the headings to Dar in a book-appropriate manner. At the instruction of Zeev I am documenting my current workflow for anyone to take up where I left off.

# Instructions for getting Sefaria running on the localhost:

Note: This was tested on Archlinux as of July 4th 2022. Instructions for Windows may differ slightly.

Sefaria is a fellow open-source Jewish database of Torah texts. To prevent doubling work we cooperate on digitizing texts. We currently need an exporter for Sefaria to convert from it’s native format to Dar which is what I was working on. In order to begin work on that Sefaria will need to be installed on one’s local computer so one can modify the server-side software and not merely accessible via the website. The following is required before beginning:


    * libffi7: This is a necessary library for Sefaria (I think because of it’s use of Cython but not sure). On Arch Linux it is available on the AUR. It’s available in many software repositories and can easily be found for Windows.

    * Python 3.9.4: The specific version of Cython listed in the requirements.txt is incompatible with any later version of Python. Make sure to install this specific version of Python.

    * Mongodb especially the Mongo Daemon (mongod) – This is what Sefaria uses to store texts in the database

    * Gettext – This is used by the Django web server Sefaria uses to manage localizations.

    * Node and npm (see here for Windows and Mac instructions: https://kinsta.com/blog/how-to-install-node-js/) . This is software required for various Javascript functions Sefaria uses. It should be available in your Linux distro’s repository by default.

    * Anything else listed in requirements.txt on the Sefaria project once you’ve cloned (this will be explained momentarily) it to your computer. This can be done via pip install -r requirements.txt

__________________________________________________________
1. Git is a software that allows for easily tracking changes to code, having multiple branches etc. Use git to clone the Sefaria   project by opening up a terminal (or on Windows the command prompt) AFTER installing git on your computer by running 
    
    git clone https://github.com/Zacharymk1213/Sefaria-Project-Dar/tree/master/sefaria

for my modified version. From within that directory run

    pip install -r requirements.txt
to install all necessary Python libraries

2. From the Sefaria-project directory created by the Git command enter the Sefaria directory and copy local_settings_example.py and paste it as local_settings.py in the same directory. Then edit it in a text editor and replace placeholder values with ones matching your environment. (This depends on what backend you want etc. Unless you need to authenticate with Mongo SEFARIA_DB_USER and SEFARIA_DB_PASSWORD can be left blank. (The only thing that might need to be changed is the path to the database file. Just make sure any directory listed exists.)

3. Create a log directory under the Sefaria-project directory you created earlier with Git and ensure the server has write access to it by running 

    chmod 777 log

(for Linux and Mac OS) or perhaps use Icacls if you have any issues on Windows (which I doubt you will) https://stackoverflow.com/questions/2928738/how-to-grant-permission-to-users-for-a-directory-using-command-line-in-windows

4. Run the mongodb daemon using the following command: 

    Mongod

If necessary use `sudo`. If on Windows, if you need to execute the Command Prompt you’re using to run Mongo as an administrator. You can do this by either right-clicking on the Command Prompt on the Start Menu and clicking run as administrator and clicking yes to the UAC prompt or by pressing the windows key, typing cmd and then pressing ctrl+shift+enter on the first result and then clicking yes to the UAC prompt.

5. Download either the small (https://storage.googleapis.com/sefaria-mongo-backup/dump_small.tar.gz) or large (https://storage.googleapis.com/sefaria-mongo-backup/dump.tar.gz) dumps. After unzipping the dump (Linux and Mac should support it by default as should eg. 7-zip on Windows. If you have any issues refer to: http://magma.maths.usyd.edu.au/magma/faq/extract for Mac, https://wiki.haskell.org/How_to_unpack_a_tar_file_in_Windows for Windows or https://phoenixnap.com/kb/extract-tar-gz-files-linux-command-line for Linux).

6. Once you have the .tar.gz extracted enter the folder created where you’ll see a folder called dump. From here open a command prompt in that directory. (Just use cd to navigate there see for reference:https://en.wikipedia.org/wiki/Directory_structure for Windows. For Mac and Linux see: https://en.wikipedia.org/wiki/Unix_filesystem#Conventional_directory_layout.) From here run:

    mongorestore –-drop

which will create a Mongo database called sefaria. If you used the smaller dump, dump_small.tar.gz, create an empty collection inside the sefaria database called history.(See for reference:https://www.mongodb.com/docs/manual/reference/method/db.createCollection/)  This can be done using the mongo client shell or a GUI you have installed, such as MongoDB Compass. (If you don’t know how or don’t want to do this just use the bigger dump.)

7. For local development, the default test keys would suffice. The warning can be suppressed by uncommenting the following in the local_settings.py file:

    SILENCED_SYSTEM_CHECKS = ['captcha.recaptcha_test_key_error']

manage.py is used to run and manage the local server. It is located in the Sefaria-Project created by Git. 

8. Download required Javascript libraries and get development tools by running from the Sefaria-project directory

    npm install; npm run setup

(use sudo or run with administrator permissions if necessary) 

9. To get the site running, you need to bundle the Javascript with Webpack. To do this run:

    npm run build-client

to bundle once. To watch the Javascript for changes and automatically rebuild, run:

    npm run watch-client 

10. Run the development server with 

    python manage.py runserver
        
from the Sefaria-project directory. If necessary use sudo or launch it from a command prompt session with Administrator permissions. 



# How to Export from Sefaria's database

Since Sefaria is a Django web app you need to run manage.py shell (if needed with sudo) in order to use Sefaria’s Django web scripts. Then run:

    from sefaria.export import make_text_to_dar

    from sefaria.export import prepare_merged_text_for_export

    from sefaria.model import text

From there you can call the functions. Relevant functions you’ll need in export.py are:

----------------------------------------------------------------------------------------

* prepare_merged_text_for_export(title, lang=None) - Creates a doc from the default version of a text, given its title. If there's no complete version it will create as complete a merged version as possible.

* The title is just the name given on Sefaria.(eg. Genesis, Maamar Zohar HaRakia, Sanhedrin etc.) 

* The convert_to_text functions and the dar conversion function I made based on that require the doc created by prepare_merged_text_for_export. To call them save the output of prepare_merged_text_for_export and then invoke make_text_to_dar passing the doc object you created via prepare_merged_text_for_export as the paramter. When that is done either write it to the disk from the variable you executed it to save as or (if you’re lazy like me) print it using:

    print($variable_name)

* and then just copy the ENTIRE output it to a blank plaintext document.
__________________________________________________________

# Usage of my Script

In the Sefaria-project folder you’ll find a script called pninim_sefaria_conversion.py. It is a normal python script and needn’t be executed via Django. It has two functions so far (more were planned to properly convert different types of books):

1. convert_to_hebrew – This function takes a number as its parameter such as convert_to_hebrew(81) and then converts this to a   Gematria value that it returns.

2. inplace_change – This takes the following as parameter(filename, old string to replace, new string to replace the old string). Pass the filename of the file you saved the Sefaria stuff in for the filename. The file will be modified according to the Pninim standard. Warning: this has only been tested on Tanakh texts and because of how it was written is not expected to work properly on other texts.

# Technical information

If you’re not developing the exporter or working with my old code you can safely ignore all of this. This is documenting what I was doing for the sake of any developer who takes over this project. This documents my work as well as any undone tasks and things I do not know. The script’s functions work as follows:

## Script Functions

### Convert_to_Hebrew

This function works by taking an integer. Then it has a loop for every number from greatest to heighest to generate the Gematria. For instance, for the value 821 the following would occur:

    • 821 would be divided by 400 twice and 800 removed from the number to generate תת.
    • The number would then be divided by 20 and this 20 would be removed adding כ to the Gematria string.
    • The number would then have 1 subtracted and א added to the Gematria string.

This Gematria string is then returned.

### inplace_change

This script takes in a file along with an old string to replace and a new one to replace it with. The following steps take place:

    • A boolean variable called is_top_removed is created to keep track of whether Sefaria generated info from the top has been removed from the document.
    • The file is read into memory in read only mode and then a list is made of the file for modification. The file is then closed.
    • The file is read into memory in read+ mode allowing for it to be written to.
    • Then a for loop is run on every item of this list that does as follows:
    • Checks if the current item is the old_string variable passed to inplace_change (eg. “Chapter”). If so it checks whether is_top_removed is set to True. If it is not, it iterates through every element of the list deleting them until it comes upon one with the same value to the old_string. The index of the loop for the list is then reset to 0. Additionally, if the current item is the same as the old_string the appropriate position in s_list (string list) is changed to %פרק in accordance with dar specifications. It’ll also try to replace the next element which would be a chapter number with a Hebrew Gematria if possible and then setting the index after that to be a newline character. If this fails it will return 1 to indicate a failure.
    • It then checks whether the last character of the list item is a sof-pasuk. It then tries to see if the next list item is “{פ}” and  then checks whether the next list item after that is “{ס}”. If this is true, it makes the newline after the “{ס}”. If this is false it makes the newline after the {פ}. If only the {ס} is found it makes the newline after the {ס}. If neither are found it makes the newline after the sof-pasuk.
    • It then checks whether the item consists of “*׀*” or “|” and if so it deletes the item because that’s just remaining junk from Sefaria.
    • It then converts the list to a string with a space between each item and writes this string to the file.
Errors I made and bugs that need to be fixed:

## Errors, bugs and missing features.
1. I made a horrible mistake in the prepare_text_for_dar function in using Regex to parse HTML instead of a proper HTML parser. That function needs to be almost entirely rewritten to use a proper HTML parser. See the first answer at https://stackoverflow.com/questions/1732348/regex-match-open-tags-except-xhtml-self-contained-tags which was sent to me.

2. Instead of using %פרק  it should probably use new_string so the function is more generic and adaptable.

3. Some way needs to be figured out to automatically add א) and things like that since the text record doesn’t have them by default and many things don’t have predictable features like Tanakh which has a sof pasuk. Perhaps one could use Sefaria’s CSV generated spreadsheets or JSONs.

4. The Hebrew Title needs to be used as the name for exported files and categories.

5. The Sefaria export script needs to be integrated with the second script.

6. The <i class=”footnote”></i> tags need to be handled correctly. Currently because of the Regex the </i> is being deleted besides for the / and the first tag is being removed. They should be moved to the bottom of the chapter.

7. Backlinks need to be handled

8. Certain elements in Sefaria are not handled at all by the Dar standard. They are at present deleted

9. Every Chapter, Siman etc. should be divided into a separate file.

10. The Index needs to be automatically created based on Sefaria data.

11. Figure out how to use the prepare_text_for_export(text) function in export.py. The description by Lev Israel is “creates a doc of a particular version, given a dictionary record of a version. (If I'm not mistaken, you should be able to get that from Version().contents())”

12. Develop the script so that different books are parsed differently (eg. for shulchan aruch it replaces siman instead of chapter)
